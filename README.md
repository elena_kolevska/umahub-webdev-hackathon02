# UmaHub Hackathon 02
#### 23th of June 2018


## The task
You need to recreate the following website:
[https://ibb.co/eAYSaT](https://ibb.co/eAYSaT)  (big screens)
[https://ibb.co/ek6fvT](https://ibb.co/ek6fvT) (Small screens)

## Colours:
Text: `#9aa7af` (gray)  
Highlights: `#56bc94` (green)  
But feel free to choose your own palette.

## Breakpoints
In this exercise we will use only one breakpoint at 700px.

## Adding icons to your website
Font awesome is one of the easiest and most popular ways to add icons to your website. You just need to include the toolkit by adding the following line in your `head` block:

```<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">```

Important note: This link is valid at the moment (June 2018), but if you’re reading this file later you should grab a “fresh” link and version directly from the FontAwesome page. Go to [https://fontawesome.com](https://fontawesome.com]), click “How to use” (the website structure might change in the future though) and find the CDN (Content Delivery Network) link.

Then when you want to add an icon in your site just include this line:
`<i class="{PREFIX} fa-{ICON-NAME}"></i>`

The prefix is almost always `fas`, except for icons of brands. In that case it’s `fab`. 
You can choose your icon from this collection:
[https://fontawesome.com/icons](https://fontawesome.com/icons)

The small name by the icon is your `{ICON-NAME}`. 
For example: I want to get an icon of facebook’s logo, for a link to my facebook page.  Should go to the above link, type facebook in the searchbox and choose one icon icon from the result set. Let’s say we want to choose the one that says `facebok-f`. To include that icon in our website we need to add the following code:
```<i class=”fab fa-facebook-f”></i>```

If we want to add an icon of a credit card, we just start typing “card” in the same search box and find the icon we need. Let’s say it’s “credit-card”. We add it in our code like this:

```<i class=”fas fa-credit-card”></i>```



## How to make an image fit your div (responsive images)
```.img-fluid {  
	max-width: 100%;
	height: auto;
}```  
Tip: After looking at it and understanding it, you can copy this class in your utility classes and use it by simply applying it to the image


## Choose nice font pairs
https://fontpair.co/


## Images
Choose your images from [https://unsplash.com](https://unsplash.com).  
For the header you need to have 2 versions of the image, a big one and a small one. The big one should be 1500px wide and the small one should be 500px wide.


## Tips on remote pair programming
Pair Programming is a pattern where two or more developers work together on one task. One developer is the Driver, and the others are the Navigators who help the Driver with directions.

- Listen more than you speak
- Ask questions instead of issuing commands
- Switch roles every 25 minutes
- Don’t interrupt
- Give compliments to good ideas
- Find a way to say “yes, and” instead of “no”.
- Find your partner’s strong points and try to complement them with yours.
- Make a plan! Split the task in a few smaller tasks and celebrate each time you complete one of them
- Take breaks
- If you’re the “driver” talk a lot, explain what you’re doing and why you’re doing it in every step of the way. 
- Ask if the team/pair agrees on an implementation before coding it out.

# Happy coding!!!